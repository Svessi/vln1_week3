#include "computerviewwindow.h"
#include "ui_computerviewwindow.h"

computerViewWindow::computerViewWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::computerViewWindow)
{
    ui->setupUi(this);
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);
    ui->buttonRemoveRelation->setEnabled(false);
    ui->lineName->setReadOnly(true);
    ui->lineType->setReadOnly(true);
    ui->lineBuildYear->setReadOnly(true);
    ui->lineWasBuilt->setReadOnly(true);
    ui->lineSelectedScientist->setReadOnly(true);
    ui->buttonRemoveRelation->setEnabled(false);
    ui->buttonRemoveRelation->setEnabled(false);
    initScientistSortComboBox();
    ui->lineSearchScientist->setPlaceholderText("Search scientist ..");
}

computerViewWindow::~computerViewWindow()
{
    delete ui;
}

void computerViewWindow::insertScientist(Computer entry)
{
    computer = entry;

    std::string wasBuilt;

    if(computer.getWasBuilt())
        wasBuilt = "Yes";
    else
        wasBuilt = "No";

    ui->lineName->setText(QString::fromStdString(computer.getName()));
    ui->lineType->setText(QString::fromStdString(computer.getType()));
    ui->lineBuildYear->setText(QString::fromStdString(computer.getYearBuilt()));
    ui->lineWasBuilt->setText(wasBuilt.c_str());

    relationList = serviceLayer.getAllScientistsByComputerId(intToString(computer.getId()));
    on_comboBoxScientistSort_activated(0);
    initSearchRelationCompleter();
}

void computerViewWindow::on_comboBoxScientistSort_activated(int index)
{
    relationList = serviceLayer.getAllScientistsByComputerId(intToString(computer.getId()));
    std::string searchInput = ui->lineSearchScientist->text().toStdString();
    std::list<Scientist> list;

    if(index == 0)//Order by nothing
        list = serviceLayer.getScientistsOrderedBy("","ASC",searchInput);

    else if(index == 1) //Order by Name
        list = serviceLayer.getScientistsOrderedBy("Name","ASC",searchInput);

    else if(index == 2)//Order by gender
        list = serviceLayer.getScientistsOrderedBy("Gender", "ASC",searchInput);

    else if(index == 3)//Order by birth
        list = serviceLayer.getScientistsOrderedBy("DateOfBirth","ASC",searchInput);

    else if(index == 4)//Order by death
        list = serviceLayer.getScientistsOrderedBy("DateOfDeath","ASC",searchInput);

    list = filterOutUnrelated(list);
    displayRelationList(list);
}

std::list<Scientist> computerViewWindow::filterOutUnrelated(std::list<Scientist> list)
{
    std::list<Scientist> newList;
    std::list<Scientist>::iterator itList = list.begin();
    std::list<Scientist>::iterator endList = list.end();
    std::list<Scientist>::iterator endRelation = relationList.end();
    Scientist listScientist, relationScientist;

    for(; itList != endList; itList++)
    {
        listScientist = *itList;

        for(std::list<Scientist>::iterator itRelation = relationList.begin(); itRelation != endRelation; itRelation++)
        {
            relationScientist = *itRelation;

            if(listScientist.getId() == relationScientist.getId())
            {
                newList.push_back(listScientist);
                break;
            }
        }
    }

    return newList;
}

void computerViewWindow::initScientistSortComboBox()
{
    ui->comboBoxScientistSort->clear();
    ui->comboBoxScientistSort->addItem("Sort by ..");
    ui->comboBoxScientistSort->addItem("Name");
    ui->comboBoxScientistSort->addItem("Gender");
    ui->comboBoxScientistSort->addItem("Birth year");
    ui->comboBoxScientistSort->addItem("Death Year");
}

void computerViewWindow::initSearchRelationCompleter()
{
    std::list<Scientist> list = relationList;
    std::list<Scientist>::iterator it = list.begin();
    std::list<Scientist>::iterator end = list.end();
    Scientist temp;
    QStringList availibleCompletions;

    for(; it != end; it++)
    {
        temp = *it;

        if(!availibleCompletions.contains(temp.getName().c_str(), Qt::CaseInsensitive))//If the same name is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getName());//Insert the name of the scientist to the completer

        if(!availibleCompletions.contains(temp.getGender().c_str(), Qt::CaseInsensitive))//If the same gender is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getGender());//Insert the gender of the scientist to the completer

        if(!availibleCompletions.contains(temp.getDateOfBirth().c_str(), Qt::CaseInsensitive))//If the same year is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getDateOfBirth());//Insert the birth year of the scientist to the completer

        if(!availibleCompletions.contains(temp.getDateOfDeath().c_str(), Qt::CaseInsensitive))//If the same year is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getDateOfDeath());//Insert the death year of the scientist to the completer
    }

    scientistSearchCompleter = new QCompleter(availibleCompletions, this);
    scientistSearchCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineSearchScientist->setCompleter(scientistSearchCompleter);
}

void computerViewWindow::displayRelationList(std::list<Scientist> list)
{
    std::list<Scientist>::iterator it = list.begin();
    std::list<Scientist>::iterator end = list.end();
    Scientist temp;
    bool isSelectedInList = false;
    int rowCount = 0;

    ui->tableScientists->clearContents();
    ui->tableScientists->setRowCount(list.size());
    relationList.clear();

    for(; it != end; it++, rowCount++)
    {
        temp = *it;
        relationList.push_back(temp);

        ui->tableScientists->setItem(rowCount, 0, new QTableWidgetItem(temp.getName().c_str()));
        ui->tableScientists->setItem(rowCount, 1, new QTableWidgetItem(temp.getGender().c_str()));
        ui->tableScientists->setItem(rowCount, 2, new QTableWidgetItem(temp.getDateOfBirth().c_str()));
        ui->tableScientists->setItem(rowCount, 3, new QTableWidgetItem(temp.getDateOfDeath().c_str()));

        if(temp.getId() == selectedScientist.getId())
        {
            ui->tableScientists->setCurrentCell(rowCount,0);
            isSelectedInList = true;
        }
    }

    if(!isSelectedInList)
    {
        selectedScientist = Scientist();
        ui->lineSelectedScientist->clear();
        ui->buttonViewScientist->setEnabled(false);
        ui->buttonRemoveRelation->setEnabled(false);
    }
}

void computerViewWindow::on_tableScientists_clicked(const QModelIndex &index)
{
    std::list<Scientist>::iterator it = relationList.begin();
    std::advance(it, index.row());
    selectedScientist = *it;

    ui->lineSelectedScientist->setText(selectedScientist.getName().c_str());
    ui->buttonViewScientist->setEnabled(true);
    ui->buttonRemoveRelation->setEnabled(true);
}

void computerViewWindow::on_lineSearchScientist_textChanged()
{
    int index = ui->comboBoxScientistSort->currentIndex();
    on_comboBoxScientistSort_activated(index);
}

void computerViewWindow::on_buttonRemoveRelation_clicked()
{
    serviceLayer.removeConnection(intToString(selectedScientist.getId()), intToString(computer.getId()));
    relationList = serviceLayer.getAllScientistsByComputerId(intToString(computer.getId()));
    initSearchRelationCompleter();
    on_comboBoxScientistSort_activated(ui->comboBoxScientistSort->currentIndex());
}

void computerViewWindow::on_buttonClose_clicked()
{
    this->close();
}

void computerViewWindow::on_buttonRemoveComputer_clicked()
{
    std::string message = "Deleting " + computer.getName() + " means all its relations will be removed.\n"
                          + "Are you Sure you want to continue?";

    QMessageBox::StandardButton buttonClicked;

    buttonClicked = QMessageBox::warning(this,"Warning",QString::fromStdString(message),QMessageBox::Yes | QMessageBox::No);

    if(buttonClicked == QMessageBox::Yes)
    {
        serviceLayer.removeComputer(intToString(computer.getId()));
        this->close();
    }
}

void computerViewWindow::on_buttonViewScientist_clicked()
{
    scientistViewWindow viewWindow;
    viewWindow.insertScientist(selectedScientist);
    viewWindow.exec();

    if(!isComputerStillInRepo(computer.getId()))
    {
        std::string message = "This computer has been deleted.";
        QMessageBox::warning(this,"Message",message.c_str());
        on_buttonClose_clicked();
    }

    on_comboBoxScientistSort_activated(ui->comboBoxScientistSort->currentIndex());
}

bool computerViewWindow::isComputerStillInRepo(int computerID)
{
    std::list<Computer> computerList = serviceLayer.getAllComputers();

    std::list<Computer>::iterator it = computerList.begin();
    std::list<Computer>::iterator end = computerList.end();
    Computer temp;

    for(; it != end; it++)
    {
        temp = *it;
        if(computerID == temp.getId())
            return true;
    }

    return false;
}

void computerViewWindow::on_tableScientists_customContextMenuRequested(const QPoint &pos)
{
    QModelIndex index = ui->tableScientists->indexAt(pos);

    if(index.row() >= 0)
    {//If right click on item in table
        QMenu menu;
        on_tableScientists_clicked(index);//Make a selection selection
        menu.addAction(ui->actionView_Scientist);
        menu.addAction(ui->actionRemove_relation);
        menu.exec(QCursor::pos());
    }
}

void computerViewWindow::on_actionView_Scientist_triggered()
{
    on_buttonViewScientist_clicked();
}

void computerViewWindow::on_actionRemove_relation_triggered()
{
    on_buttonRemoveRelation_clicked();
}

void computerViewWindow::on_tableScientists_doubleClicked()
{
    on_buttonViewScientist_clicked();
}
