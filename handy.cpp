#include "handy.h"

std::string intToString(int integer)
{
    std::stringstream out;
    out << integer;
    return out.str();
}

bool isAllDigits(std::string number)
{
    int stringSize = number.length();

    for(int i = 0; i < stringSize; i++)
    {
        if(!isdigit(number[i]))
            return false;
    }

    return true;
}

std::list<std::string> splitString(std::string str)
{
    std::list<std::string> returnList;
    int stringLength = str.length();
    std::string temp = "";

    for(int i = 0; i < stringLength; i++)
    {
        if(isspace(str[i]) && temp.length() > 0)
        {
            returnList.push_back(temp);
            temp = "";
            continue;
        }
        else if(isspace(str[i]))
            continue;

        temp += str[i];
    }

    if(temp.length())
        returnList.push_back(temp);

    return returnList;
}
