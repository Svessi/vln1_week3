#ifndef SCIENTISTVIEWWINDOW_H
#define SCIENTISTVIEWWINDOW_H

#include <QDialog>
#include "scientist.h"
#include "scienceservice.h"
#include "computer.h"
#include <list>
#include "handy.h"
#include <QCompleter>
#include <QMenu>
#include <QMessageBox>
#include "computerviewwindow.h"

namespace Ui {
class scientistViewWindow;
}

class scientistViewWindow : public QDialog
{
    Q_OBJECT

public:
    explicit scientistViewWindow(QWidget *parent = 0);
    ~scientistViewWindow();

    void insertScientist(Scientist entry);
private slots:
    void on_comboBoxComputerSort_activated(int index);

    void on_lineSearchComputer_textChanged();

    void on_buttonRemoveRelation_clicked();

    void on_buttonClose_clicked();

    void on_buttonRemoveScientist_clicked();

    void on_buttonViewComputer_clicked();

    void on_tableComputers_clicked(const QModelIndex &index);

    void on_tableComputers_customContextMenuRequested(const QPoint &pos);

    void on_actionView_computer_triggered();

    void on_actionRemove_relation_triggered();

    void on_tableComputers_doubleClicked();

private:
    Ui::scientistViewWindow *ui;
    ScienceService serviceLayer;
    Scientist scientist;
    std::list<Computer> relationList;
    Computer selectedComputer;
    QCompleter *computerSearchCompleter;
    void displayRelationList(std::list<Computer> list);
    void initComputerSortComboBox();
    std::list<Computer> filterOutUnrelated(std::list<Computer> list);
    void initSearchRelationCompleter();
    bool isScientistStillInRepo(int scientistID);
};

#endif // SCIENTISTVIEWWINDOW_H
