#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <sstream>
#include <QCompleter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->buttonRelate->setEnabled(false);
    ui->buttonScientistView->setEnabled(false);
    ui->buttonComputerView->setEnabled(false);
    ui->lineSelectedScientist->setReadOnly(true);
    ui->lineSelectedComputer->setReadOnly(true);
    ui->lineScientistSearch->setPlaceholderText("Search scientist ..");
    ui->lineComputerSearch->setPlaceholderText("Search computer ..");
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);

    on_comboBoxScientistSort_activated(0);
    on_comboBoxComputerSort_activated(0);
    initScientistSortComboBox();
    initComputerSortComboBox();
    initScientistSearchCompleter();
    initComputersearchCompleter();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initScientistSortComboBox()
{
    ui->comboBoxScientistSort->clear();
    ui->comboBoxScientistSort->addItem("Sort by ..");
    ui->comboBoxScientistSort->addItem("Name");
    ui->comboBoxScientistSort->addItem("Gender");
    ui->comboBoxScientistSort->addItem("Birth year");
    ui->comboBoxScientistSort->addItem("Death year");
}

void MainWindow::initComputerSortComboBox()
{
    ui->comboBoxComputerSort->clear();
    ui->comboBoxComputerSort->addItem("Sort by ..");
    ui->comboBoxComputerSort->addItem("Name");
    ui->comboBoxComputerSort->addItem("Type");
    ui->comboBoxComputerSort->addItem("Build year");
    ui->comboBoxComputerSort->addItem("Was built");
}

void MainWindow::on_comboBoxScientistSort_activated(int index)
{
    std::list<Scientist> list;
    std::string searchInput = ui->lineScientistSearch->text().toStdString();

    if(index == 0)//No ordering
        list = serviceLayer.getScientistsOrderedBy("","ASC",searchInput);

    else if(index == 1) //Order by Name
        list = serviceLayer.getScientistsOrderedBy("Name","ASC",searchInput);

    else if(index == 2)//Order by gender
        list = serviceLayer.getScientistsOrderedBy("Gender", "ASC",searchInput);

        else if(index == 3)//Order by birth
        list = serviceLayer.getScientistsOrderedBy("DateOfBirth","ASC",searchInput);

    else if(index == 4)//Order by death
        list = serviceLayer.getScientistsOrderedBy("DateOfDeath","ASC",searchInput);

    displayScientistList(list);
}

void MainWindow::on_comboBoxComputerSort_activated(int index)
{
    std::string searchInput = ui->lineComputerSearch->text().toStdString();

    std::list<Computer> list;

    if(index == 0)//No ordering
        list = serviceLayer.getComputersOrderedBy("","ASC",searchInput);

    else if(index == 1) //Order by Name
        list = serviceLayer.getComputersOrderedBy("Name","ASC",searchInput);

    else if(index == 2)//Order by Type
        list = serviceLayer.getComputersOrderedBy("Type", "ASC",searchInput);

    else if(index == 3)//Order by buildYear
        list = serviceLayer.getComputersOrderedBy("YearBuilt","ASC",searchInput);

    else if(index == 4)//Order by wasBuilt
        list = serviceLayer.getComputersOrderedBy("WasBuilt","ASC",searchInput);

    displayComputerList(list);
}

void MainWindow::displayScientistList(std::list<Scientist> list)
{
    std::list<Scientist>::iterator it = list.begin();
    std::list<Scientist>::iterator end = list.end();
    Scientist temp;
    bool isSelectedInList = false;
    int rowCount = 0;

    ui->tableScientists->clearContents();
    ui->tableScientists->setRowCount(list.size());

    scientistList.clear();

    for(; it != end; it++,rowCount++)
    {
        temp = *it;
        scientistList.push_back(temp);

        ui->tableScientists->setItem(rowCount, 0, new QTableWidgetItem(temp.getName().c_str()));
        ui->tableScientists->setItem(rowCount, 1, new QTableWidgetItem(temp.getGender().c_str()));
        ui->tableScientists->setItem(rowCount, 2, new QTableWidgetItem(temp.getDateOfBirth().c_str()));
        ui->tableScientists->setItem(rowCount, 3, new QTableWidgetItem(temp.getDateOfDeath().c_str()));

        if(temp.getId() == selectedScientist.getId())
        {//If the selected scientist is still in the list
            ui->tableScientists->setCurrentCell(rowCount,0);//Keep selection on that item
            isSelectedInList = true;
        }
    }

    if(!isSelectedInList)
    {//If the selected scientist has been filtered out
        selectedScientist = Scientist(); //clear selection
        ui->lineSelectedScientist->clear(); //clear the selection line
        ui->tableScientists->clearSelection();
        ui->buttonRelate->setEnabled(false);
        ui->buttonScientistView->setEnabled(false);
        ui->labelRelation->setText("Select a Scientist and a Computer from the lists to relate");
    }
}

void MainWindow::displayComputerList(std::list<Computer> list)
{
    std::list<Computer>::iterator it = list.begin();
    std::list<Computer>::iterator end = list.end();
    Computer temp;
    bool isSelectedInList = false;
    int rowCount = 0;
    QString wasBuilt;

    ui->tableComputers->clearContents();
    ui->tableComputers->setRowCount(list.size());

    computerList = std::list<Computer>();

    for(; it != end; it++, rowCount++)
    {
        temp = *it;
        computerList.push_back(temp);

        if(temp.getWasBuilt())
            wasBuilt = "Yes";
        else
            wasBuilt = "No";

        ui->tableComputers->setItem(rowCount, 0, new QTableWidgetItem(temp.getName().c_str()));
        ui->tableComputers->setItem(rowCount, 1, new QTableWidgetItem(temp.getType().c_str()));
        ui->tableComputers->setItem(rowCount, 2, new QTableWidgetItem(temp.getYearBuilt().c_str()));
        ui->tableComputers->setItem(rowCount, 3, new QTableWidgetItem(wasBuilt));

        if(temp.getId() == selectedComputer.getId())
        {
            ui->tableComputers->setCurrentCell(rowCount,0);
            isSelectedInList = true;
        }
    }

    if(!isSelectedInList)
    {//If the computer has been filtered out
        selectedComputer = Computer();//clear selection
        ui->lineSelectedComputer->clear();//clear selection line
        ui->tableComputers->clearSelection();
        ui->buttonRelate->setEnabled(false);
        ui->buttonComputerView->setEnabled(false);
        ui->labelRelation->setText("Select a Scientist and a Computer from the lists to relate");
    }
}

void MainWindow::on_tableScientists_clicked(const QModelIndex &index)
{//The scientist list is ordered in the same way as the table
    std::list<Scientist>::iterator it = scientistList.begin();
    std::advance(it, index.row());//get the position of the scientist that was clicked on
    selectedScientist = *it; //set the scientist as selected

    ui->lineSelectedScientist->setText(selectedScientist.getName().c_str());//Display the name of the scientist on the scientist selection line
    ui->buttonScientistView->setEnabled(true); //button that will allow the user to view the information of the scientist

    if(ui->lineSelectedComputer->text().isEmpty())//If the computer selection line is empty then relation operation can´t be made
        ui->buttonRelate->setEnabled(false);
    else
        ui->buttonRelate->setEnabled(true);

    ui->labelRelation->setText("Select a Scientist and a Computer from the lists to relate");
}

void MainWindow::on_tableComputers_clicked(const QModelIndex &index)
{
    std::list<Computer>::iterator it = computerList.begin();
    std::advance(it, index.row());//Get the position of the computer that was clicked on
    selectedComputer = *it; // set it as selected

    ui->lineSelectedComputer->setText(selectedComputer.getName().c_str());//Put the name into the computer selection line
    ui->buttonComputerView->setEnabled(true);//button that will allow the user to view the information of the selected computer

    if(ui->lineSelectedScientist->text().isEmpty())//If the scientist selection line is empty then relation operation can´t be made
        ui->buttonRelate->setEnabled(false);
    else
        ui->buttonRelate->setEnabled(true);

    ui->labelRelation->setText("Select a Scientist and a Computer from the lists to relate");
}

void MainWindow::on_buttonRelate_clicked()
{
    serviceLayer.addConnection(intToString(selectedScientist.getId()), intToString(selectedComputer.getId()));
    std::string message = selectedScientist.getName() + " and " + selectedComputer.getName() + " have been related";
    ui->labelRelation->setText(message.c_str());
}

void MainWindow::on_lineScientistSearch_textChanged()
{
    int index = ui->comboBoxScientistSort->currentIndex(); //get the selection of the combobox
    on_comboBoxScientistSort_activated(index); //call the sorting with that selection
    //The comboBoxScientistSort function will get check the lineEdit so they are in sync
}

void MainWindow::on_lineComputerSearch_textChanged()
{
    int index = ui->comboBoxComputerSort->currentIndex();
    on_comboBoxComputerSort_activated(index);
}

void MainWindow::initScientistSearchCompleter()
{
    std::list<Scientist> list = serviceLayer.getAllScientists();
    std::list<Scientist>::iterator it = list.begin();
    std::list<Scientist>::iterator end = list.end();
    Scientist temp;
    QStringList availibleCompletions;

    for(; it != end; it++)
    {
        temp = *it;

        if(!availibleCompletions.contains(temp.getName().c_str(), Qt::CaseInsensitive))//If the same name is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getName());//Insert the name of the scientist to the completer

        if(!availibleCompletions.contains(temp.getGender().c_str(), Qt::CaseInsensitive))//If the same gender is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getGender());//Insert the gender of the scientist to the completer

        if(!availibleCompletions.contains(temp.getDateOfBirth().c_str(), Qt::CaseInsensitive))//If the same year is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getDateOfBirth());//Insert the birth year of the scientist to the completer

        if(!availibleCompletions.contains(temp.getDateOfDeath().c_str(), Qt::CaseInsensitive))//If the same year is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getDateOfDeath());//Insert the death year of the scientist to the completer
    }

    scientistSearchCompleter = new QCompleter(availibleCompletions, this);
    scientistSearchCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineScientistSearch->setCompleter(scientistSearchCompleter);
}

void MainWindow::initComputersearchCompleter()
{
    std::list<Computer> list = serviceLayer.getAllComputers();
    std::list<Computer>::iterator it = list.begin();
    std::list<Computer>::iterator end = list.end();
    Computer temp;
    QStringList availibleNames;

    for(; it != end; it++)
    {
        temp = *it;

        if(!availibleNames.contains(temp.getName().c_str(), Qt::CaseInsensitive))//If this name is not already in the complerter
            availibleNames << QString::fromStdString(temp.getName());//Then add it to the completer

        if(!availibleNames.contains(temp.getType().c_str(), Qt::CaseInsensitive))//If this type is not already in the completer
            availibleNames << QString::fromStdString(temp.getType());//Then add it to the completer

        if(!availibleNames.contains(temp.getYearBuilt().c_str(), Qt::CaseInsensitive))//If this year is not already in the completer
            availibleNames << QString::fromStdString(temp.getYearBuilt());//Then add it to the completer
    }

    computerSearchCompleter = new QCompleter(availibleNames, this);
    computerSearchCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineComputerSearch->setCompleter(computerSearchCompleter);
}

void MainWindow::on_buttonScientistView_clicked()
{
    scientistViewWindow viewWindow;//Make a modal dialog
    viewWindow.insertScientist(selectedScientist);// insert the scientist that should be displayed in the dialog
    viewWindow.exec();

    //Updating tables and completers, since some changes might have been made to the repository within the dialog
    initScientistSearchCompleter();
    initComputersearchCompleter();

    on_comboBoxScientistSort_activated(ui->comboBoxScientistSort->currentIndex());
    on_comboBoxComputerSort_activated(ui->comboBoxComputerSort->currentIndex());
}

void MainWindow::on_actionAdd_Scientist_triggered()
{
    addScientistWindow window;//Make a modal dialog
    window.exec();

    //Update the table and completer since a new scientist might have been added
    on_comboBoxScientistSort_activated(ui->comboBoxScientistSort->currentIndex());
    initScientistSearchCompleter();
}

void MainWindow::on_actionAdd_Computer_triggered()
{
    addcomputerwindow window;//Make a modal dialog
    window.exec();

    //Update the table and completer since a new computer might have been added
    on_comboBoxComputerSort_activated(ui->comboBoxComputerSort->currentIndex());
    initComputersearchCompleter();
}

void MainWindow::on_buttonComputerView_clicked()
{
    computerViewWindow viewWindow;//make a modal dialog
    viewWindow.insertScientist(selectedComputer);//insert the computer that should be displayed in the dialog
    viewWindow.exec();

    //Updating tables and completers, since some changes might have been made to the repository within the dialog
    initScientistSearchCompleter();
    initComputersearchCompleter();

    on_comboBoxScientistSort_activated(ui->comboBoxScientistSort->currentIndex());
    on_comboBoxComputerSort_activated(ui->comboBoxComputerSort->currentIndex());
}

void MainWindow::on_tableComputers_customContextMenuRequested(const QPoint &pos)
{
    QModelIndex index = ui->tableComputers->indexAt(pos);
    QMenu menu;

    if(index.row() >= 0)
    {//If right click on item in table
        on_tableComputers_clicked(index);//Make a selection selection
        menu.addAction(ui->actionView_computer);
        menu.addAction(ui->actionDelete_computer);
        menu.addAction(ui->actionEdit_Computer);
    }

    menu.addSeparator();
    menu.addAction(ui->actionAdd_Computer);
    menu.exec(QCursor::pos());
}

void MainWindow::on_tableScientists_customContextMenuRequested(const QPoint &pos)
{
    QModelIndex index = ui->tableScientists->indexAt(pos);
    QMenu menu;

    if(index.row() >= 0)
    {//If right click on item in table
        on_tableScientists_clicked(index);//Make a selection selection
        menu.addAction(ui->actionView_Scientist);
        menu.addAction(ui->actionDelete_scientist);
        menu.addAction(ui->actionEdit_scientist);
    }

    menu.addSeparator();
    menu.addAction(ui->actionAdd_Scientist);
    menu.exec(QCursor::pos());
}

void MainWindow::on_actionDelete_computer_triggered()
{
    std::string message = "Deleting " + selectedComputer.getName() + " means all its relations will be removed.\n"
                          + "Are you Sure you want to continue?";

    QMessageBox::StandardButton buttonClicked;

    buttonClicked = QMessageBox::warning(this,"Warning",QString::fromStdString(message),QMessageBox::Yes | QMessageBox::No);

    if(buttonClicked == QMessageBox::Yes)
    {
        serviceLayer.removeComputer(intToString(selectedComputer.getId()));
        initComputersearchCompleter();
        on_comboBoxComputerSort_activated(ui->comboBoxComputerSort->currentIndex());
    }
}

void MainWindow::on_actionDelete_scientist_triggered()
{
    std::string thirdP = (selectedScientist.getGender() == "Male") ? "his" : "her";
    std::string message = "Deleting " + selectedScientist.getName() + " means all " + thirdP
                          + " relations will be removed.\nAre you Sure you want to continue?";

    QMessageBox::StandardButton buttonClicked;

    buttonClicked = QMessageBox::warning(this,"Warning",QString::fromStdString(message),QMessageBox::Yes | QMessageBox::No);

    if(buttonClicked == QMessageBox::Yes)
    {
        serviceLayer.removeScientist(intToString(selectedScientist.getId()));
        initScientistSearchCompleter();
        on_comboBoxScientistSort_activated(ui->comboBoxScientistSort->currentIndex());
    }
}

void MainWindow::on_actionView_computer_triggered()
{
    on_buttonComputerView_clicked();
}

void MainWindow::on_actionView_Scientist_triggered()
{
    on_buttonScientistView_clicked();
}

void MainWindow::on_tableScientists_doubleClicked()
{
    on_actionView_Scientist_triggered();
}

void MainWindow::on_tableComputers_doubleClicked()
{
    on_actionView_computer_triggered();
}

void MainWindow::on_actionEdit_Computer_triggered()
{
    addcomputerwindow editWindow;
    editWindow.setEditMode(selectedComputer);//give the window the computer info
    editWindow.exec();
    initComputersearchCompleter();
    on_comboBoxComputerSort_activated(ui->comboBoxComputerSort->currentIndex());
}

void MainWindow::on_actionEdit_scientist_triggered()
{
    addScientistWindow editWindow;
    editWindow.setEditMode(selectedScientist);//give the window the scientist info
    editWindow.exec();
    initScientistSearchCompleter();
    on_comboBoxScientistSort_activated(ui->comboBoxScientistSort->currentIndex());
}
