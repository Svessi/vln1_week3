#ifndef ADDCOMPUTERWINDOW_H
#define ADDCOMPUTERWINDOW_H

#include <QDialog>
#include "handy.h"
#include "computer.h"
#include "scienceservice.h"

namespace Ui {
class addcomputerwindow;
}

class addcomputerwindow : public QDialog
{
    Q_OBJECT

public:
    explicit addcomputerwindow(QWidget *parent = 0);
    ~addcomputerwindow();

    void setEditMode(Computer computer);
private slots:

    void on_pushButton_ok_clicked();

    void on_pushButton_cancel_clicked();

    void on_checkBox_was_built_yes_clicked();

    void on_checkBox_was_built_no_clicked();

private:
    Ui::addcomputerwindow *ui;

    ScienceService serviceLayer;
    bool editMode;
    Computer editComputer;

    void clearAddComputerError();
    bool computerInputIsValid();
};

#endif // ADDCOMPUTERWINDOW_H
