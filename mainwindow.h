#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "scientistviewwindow.h"
#include "computerviewwindow.h"
#include "addscientistwindow.h"
#include "addcomputerwindow.h"
#include "scienceservice.h"
#include "handy.h"
#include <QListWidgetItem>
#include <QMenu>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_comboBoxScientistSort_activated(int index);

    void on_comboBoxComputerSort_activated(int index);

    void on_buttonRelate_clicked();

    void on_lineScientistSearch_textChanged();

    void on_lineComputerSearch_textChanged();

    void on_buttonScientistView_clicked();

    void on_actionAdd_Scientist_triggered();

    void on_buttonComputerView_clicked();

    void on_tableScientists_clicked(const QModelIndex &index);

    void on_tableComputers_clicked(const QModelIndex &index);

    void on_actionAdd_Computer_triggered();

    void on_tableComputers_customContextMenuRequested(const QPoint &pos);

    void on_actionDelete_computer_triggered();

    void on_actionView_computer_triggered();

    void on_tableScientists_customContextMenuRequested(const QPoint &pos);

    void on_actionDelete_scientist_triggered();

    void on_actionView_Scientist_triggered();

    void on_tableScientists_doubleClicked();

    void on_tableComputers_doubleClicked();

    void on_actionEdit_Computer_triggered();

    void on_actionEdit_scientist_triggered();

private:
    Ui::MainWindow *ui;
    ScienceService serviceLayer;
    std::list<Scientist> scientistList;
    std::list<Computer> computerList;
    Scientist selectedScientist;
    Computer selectedComputer;
    QCompleter *scientistSearchCompleter;
    QCompleter *computerSearchCompleter;
    void initScientistSortComboBox();
    void initComputerSortComboBox();
    void displayScientistList(std::list<Scientist> list);
    void displayComputerList(std::list<Computer> list);
    void initScientistSearchCompleter();
    void initComputersearchCompleter();
};

#endif // MAINWINDOW_H
