#ifndef ADDSCIENTISTWINDOW_H
#define ADDSCIENTISTWINDOW_H

#include <QDialog>
#include "scientist.h"
#include "handy.h"
#include "scienceservice.h"

namespace Ui {
class addScientistWindow;
}

class addScientistWindow : public QDialog
{
    Q_OBJECT

public:
    explicit addScientistWindow(QWidget *parent = 0);
    ~addScientistWindow();

    void setEditMode(Scientist scientist);
private slots:

    void on_checkBoxMale_clicked();

    void on_checkBoxFemale_clicked();

    void on_checkBoxAliveYes_clicked();

    void on_checkBoxAliveNo_clicked();

    void on_pushButton_ok_clicked();

    void on_pushButton_cancel_clicked();

private:
    Ui::addScientistWindow *ui;

    ScienceService serviceLayer;
    Scientist editScientist;
    bool editMode;

    bool ScientistInputIsValid();
    void clearAddScientistError();

};

#endif // ADDSCIENTISTWINDOW_H
