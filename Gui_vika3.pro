#-------------------------------------------------
#
# Project created by QtCreator 2014-12-11T21:14:30
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Gui_vika3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
    scientistrepository.cpp \
    scientist.cpp \
    scienceservice.cpp \
    baserepository.cpp \
    computerrepository.cpp \
    computer.cpp \
    scientistcomputerconnections.cpp \
    scientistcomputerconnectionsrepository.cpp \
    addscientistwindow.cpp \
    scientistviewwindow.cpp \
    handy.cpp \
    computerviewwindow.cpp \
    addcomputerwindow.cpp

HEADERS  += mainwindow.h \
    scientistrepository.h \
    scientist.h \
    scienceservice.h \
    baserepository.h \
    computerrepository.h \
    computer.h \
    scientistcomputerconnections.h \
    scientistcomputerconnectionsrepository.h \
    addscientistwindow.h \
    scientistviewwindow.h \
    handy.h \
    computerviewwindow.h \
    addcomputerwindow.h

FORMS    += mainwindow.ui \
    addscientistwindow.ui \
    scientistviewwindow.ui \
    computerviewwindow.ui \
    addcomputerwindow.ui

RESOURCES += \
    guiResourceFile.qrc
