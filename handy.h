#ifndef HANDY_H
#define HANDY_H

#include <iostream>
#include <string>
#include <list>
#include <sstream>
#include <QString>

std::string intToString(int integer);
bool isAllDigits(std::string number);
std::list<std::string> splitString(std::string str);

#endif // HANDY_H
