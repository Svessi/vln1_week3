#include "scientistviewwindow.h"
#include "ui_scientistviewwindow.h"

scientistViewWindow::scientistViewWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::scientistViewWindow)
{
    ui->setupUi(this);
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);
    ui->buttonRemoveRelation->setEnabled(false);
    ui->lineName->setReadOnly(true);
    ui->lineGender->setReadOnly(true);
    ui->lineBirth->setReadOnly(true);
    ui->lineDeath->setReadOnly(true);
    ui->lineSelectedComputer->setReadOnly(true);
    ui->buttonRemoveRelation->setEnabled(false);
    ui->buttonRemoveRelation->setEnabled(false);
    initComputerSortComboBox();
    ui->lineSearchComputer->setPlaceholderText("Search computer ..");

}

scientistViewWindow::~scientistViewWindow()
{
    delete ui;
}

void scientistViewWindow::insertScientist(Scientist entry)
{
    scientist = entry;
    ui->lineName->setText(QString::fromStdString(scientist.getName()));
    ui->lineGender->setText(QString::fromStdString(scientist.getGender()));
    ui->lineBirth->setText(QString::fromStdString(scientist.getDateOfBirth()));
    ui->lineDeath->setText(QString::fromStdString(scientist.getDateOfDeath()));

    relationList = serviceLayer.getAllComputersByScientistId(intToString(entry.getId()));
    on_comboBoxComputerSort_activated(0);
    initSearchRelationCompleter();
}

void scientistViewWindow::initComputerSortComboBox()
{
    ui->comboBoxComputerSort->clear();
    ui->comboBoxComputerSort->addItem("Sort by ..");
    ui->comboBoxComputerSort->addItem("Name");
    ui->comboBoxComputerSort->addItem("Type");
    ui->comboBoxComputerSort->addItem("Build year");
    ui->comboBoxComputerSort->addItem("Was built");
}

void scientistViewWindow::on_comboBoxComputerSort_activated(int index)
{
    relationList = serviceLayer.getAllComputersByScientistId(intToString(scientist.getId()));
    std::string searchInput = ui->lineSearchComputer->text().toStdString();

    std::list<Computer> list;

    if(index == 0)//Order by nothing
        list = serviceLayer.getComputersOrderedBy("","ASC",searchInput);

    else if(index == 1) //Order by Name
        list = serviceLayer.getComputersOrderedBy("Name","ASC",searchInput);

    else if(index == 2)//Order by Type
        list = serviceLayer.getComputersOrderedBy("Type", "ASC",searchInput);

    else if(index == 3)//Order by BuildYear
        list = serviceLayer.getComputersOrderedBy("YearBuilt","ASC",searchInput);

    else if(index == 4)//Order by wasBuilt
        list = serviceLayer.getComputersOrderedBy("WasBuilt","ASC",searchInput);

    list = filterOutUnrelated(list);
    displayRelationList(list);
}

void scientistViewWindow::displayRelationList(std::list<Computer> list)
{
    std::list<Computer>::iterator it = list.begin();
    std::list<Computer>::iterator end = list.end();
    Computer temp;
    QString wasBuilt;
    bool isSelectedInList = false;
    int rowCount = 0;

    ui->tableComputers->clearContents();
    ui->tableComputers->setRowCount(list.size());
    relationList.clear();

    for(; it != end; it++, rowCount++)
    {
        temp = *it;
        relationList.push_back(temp);

        if(temp.getWasBuilt())
            wasBuilt = "Yes";
        else
            wasBuilt = "No";

        ui->tableComputers->setItem(rowCount, 0, new QTableWidgetItem(temp.getName().c_str()));
        ui->tableComputers->setItem(rowCount, 1, new QTableWidgetItem(temp.getType().c_str()));
        ui->tableComputers->setItem(rowCount, 2, new QTableWidgetItem(temp.getYearBuilt().c_str()));
        ui->tableComputers->setItem(rowCount, 3, new QTableWidgetItem(wasBuilt));

        if(temp.getId() == selectedComputer.getId())
        {
            ui->tableComputers->setCurrentCell(rowCount,0);
            isSelectedInList = true;
        }
    }

    if(!isSelectedInList)
    {
        selectedComputer = Computer();
        ui->lineSelectedComputer->clear();
        ui->tableComputers->clearSelection();
        ui->buttonViewComputer->setEnabled(false);
        ui->buttonRemoveRelation->setEnabled(false);
    }
}

std::list<Computer> scientistViewWindow::filterOutUnrelated(std::list<Computer> list)
{
    std::list<Computer> newList;
    std::list<Computer>::iterator itList = list.begin();
    std::list<Computer>::iterator endList = list.end();
    std::list<Computer>::iterator endRelation = relationList.end();
    Computer listComputer, relationComputer;

    for(; itList != endList; itList++)
    {
        listComputer = *itList;

        for(std::list<Computer>::iterator itRelation = relationList.begin(); itRelation != endRelation; itRelation++)
        {
            relationComputer = *itRelation;

            if(listComputer.getId() == relationComputer.getId())
            {
                newList.push_back(listComputer);
                break;
            }
        }
    }

    return newList;
}

void scientistViewWindow::on_tableComputers_clicked(const QModelIndex &index)
{
    std::list<Computer>::iterator it = relationList.begin();
    std::advance(it, index.row());
    selectedComputer = *it;

    ui->lineSelectedComputer->setText(selectedComputer.getName().c_str());
    ui->buttonViewComputer->setEnabled(true);
    ui->buttonRemoveRelation->setEnabled(true);
}

void scientistViewWindow::initSearchRelationCompleter()
{
    std::list<Computer> list = relationList;
    std::list<Computer>::iterator it = list.begin();
    std::list<Computer>::iterator end = list.end();
    Computer temp;
    QStringList availibleCompletions;

    for(; it != end; it++)
    {
        temp = *it;

        if(!availibleCompletions.contains(temp.getName().c_str(), Qt::CaseInsensitive))//If this name is not already in the complerter
            availibleCompletions << QString::fromStdString(temp.getName());//Then add it to the completer

        if(!availibleCompletions.contains(temp.getType().c_str(), Qt::CaseInsensitive))//If this type is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getType());//Then add it to the completer

        if(!availibleCompletions.contains(temp.getYearBuilt().c_str(), Qt::CaseInsensitive))//If this year is not already in the completer
            availibleCompletions << QString::fromStdString(temp.getYearBuilt());//Then add it to the completer
    }

    computerSearchCompleter = new QCompleter(availibleCompletions, this);
    computerSearchCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineSearchComputer->setCompleter(computerSearchCompleter);
}

void scientistViewWindow::on_lineSearchComputer_textChanged()
{
    int index = ui->comboBoxComputerSort->currentIndex();
    on_comboBoxComputerSort_activated(index);
}

void scientistViewWindow::on_buttonRemoveRelation_clicked()
{
    serviceLayer.removeConnection(intToString(scientist.getId()), intToString(selectedComputer.getId()));
    relationList = serviceLayer.getAllComputersByScientistId(intToString(scientist.getId()));
    initSearchRelationCompleter();
    on_comboBoxComputerSort_activated(ui->comboBoxComputerSort->currentIndex());
}

void scientistViewWindow::on_buttonClose_clicked()
{
    this->close();
}

void scientistViewWindow::on_buttonRemoveScientist_clicked()
{
    std::string thirdP = (scientist.getGender() == "Male") ? "his" : "her";
    std::string message = "Deleting " + scientist.getName() + " means all " + thirdP
                          + " relations will be removed.\nAre you Sure you want to continue?";

    QMessageBox::StandardButton buttonClicked;

    buttonClicked = QMessageBox::warning(this,"Warning",QString::fromStdString(message),QMessageBox::Yes | QMessageBox::No);

    if(buttonClicked == QMessageBox::Yes)
    {
        serviceLayer.removeScientist(intToString(scientist.getId()));
        this->close();
    }
}

void scientistViewWindow::on_buttonViewComputer_clicked()
{
    computerViewWindow viewWindow;
    viewWindow.insertScientist(selectedComputer);
    viewWindow.exec();

    if(!isScientistStillInRepo(scientist.getId()))
    {
        std::string message = "This scientist has been deleted.";
        QMessageBox::warning(this,"Message",message.c_str());
        on_buttonClose_clicked();
    }

    on_comboBoxComputerSort_activated(ui->comboBoxComputerSort->currentIndex());
}

bool scientistViewWindow::isScientistStillInRepo(int scientistID)
{
    std::list<Scientist> scientistList = serviceLayer.getAllScientists();

    std::list<Scientist>::iterator it = scientistList.begin();
    std::list<Scientist>::iterator end = scientistList.end();
    Scientist temp;

    for(; it != end; it++)
    {
        temp = *it;
        if(scientistID == temp.getId())
            return true;
    }

    return false;
}

void scientistViewWindow::on_tableComputers_customContextMenuRequested(const QPoint &pos)
{
    QModelIndex index = ui->tableComputers->indexAt(pos);

    if(index.row() >= 0)
    {
        QMenu menu;
        on_tableComputers_clicked(index);//Make a selection selection
        menu.addAction(ui->actionView_computer);
        menu.addAction(ui->actionRemove_relation);
        menu.exec(QCursor::pos());
    }
}

void scientistViewWindow::on_actionView_computer_triggered()
{
    on_buttonViewComputer_clicked();
}

void scientistViewWindow::on_actionRemove_relation_triggered()
{
    on_buttonRemoveRelation_clicked();
}

void scientistViewWindow::on_tableComputers_doubleClicked()
{
    on_buttonViewComputer_clicked();
}
