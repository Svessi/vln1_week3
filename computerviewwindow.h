#ifndef COMPUTERVIEWWINDOW_H
#define COMPUTERVIEWWINDOW_H

#include <QDialog>
#include "scientist.h"
#include "scienceservice.h"
#include "computer.h"
#include <list>
#include "handy.h"
#include <QCompleter>
#include <QMessageBox>
#include "scientistviewwindow.h"

namespace Ui {
class computerViewWindow;
}

class computerViewWindow : public QDialog
{
    Q_OBJECT

public:
    explicit computerViewWindow(QWidget *parent = 0);
    ~computerViewWindow();
    void insertScientist(Computer entry);

private slots:
    void on_comboBoxScientistSort_activated(int index);

    void on_lineSearchScientist_textChanged();

    void on_buttonRemoveRelation_clicked();

    void on_buttonClose_clicked();

    void on_buttonRemoveComputer_clicked();

    void on_buttonViewScientist_clicked();

    void on_tableScientists_clicked(const QModelIndex &index);

    void on_tableScientists_customContextMenuRequested(const QPoint &pos);

    void on_actionView_Scientist_triggered();

    void on_actionRemove_relation_triggered();

    void on_tableScientists_doubleClicked();

private:
    Ui::computerViewWindow *ui;
    ScienceService serviceLayer;
    Computer computer;
    std::list<Scientist> relationList;
    QCompleter* scientistSearchCompleter;
    Scientist selectedScientist;
    std::list<Scientist> filterOutUnrelated(std::list<Scientist> list);
    void initScientistSortComboBox();
    void initSearchRelationCompleter();
    void displayRelationList(std::list<Scientist> list);
    bool isComputerStillInRepo(int computerID);
};

#endif // COMPUTERVIEWWINDOW_H
