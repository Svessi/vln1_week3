#include "addcomputerwindow.h"
#include "ui_addcomputerwindow.h"

addcomputerwindow::addcomputerwindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addcomputerwindow)
{
    ui->setupUi(this);
    editMode = false;
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);
}

addcomputerwindow::~addcomputerwindow()
{
    delete ui;
}

void addcomputerwindow::setEditMode(Computer computer)
{
    editComputer = computer;
    editMode = true;

    std::list<std::string> types = splitString(computer.getType());
    std::list<std::string>::iterator it = types.begin();
    std::list<std::string>::iterator end = types.end();
    std::string type;

    for(; it != end; it++)
    {
        type = *it;
        qDebug() << type.c_str();

        if(type == "Mechanical")
            ui->checkBox_mechanical->setChecked(true);

        else if(type == "Electronical")
            ui->checkBox_electronical->setChecked(true);

        else if(type == "Transistor")
            ui->checkBox_smaravel->setChecked(true);
    }

    ui->input_name->setText(editComputer.getName().c_str());
    ui->input_year_built->setText(editComputer.getYearBuilt().c_str());

    if(editComputer.getWasBuilt())
        ui->checkBox_was_built_yes->setChecked(true);

    else
        ui->checkBox_was_built_no->setChecked(true);
}

void addcomputerwindow::on_pushButton_ok_clicked()
{
    if(computerInputIsValid())
    {//If all input is valid
        Computer temp;
        std::string type;

        if(ui->checkBox_mechanical->isChecked())
            type = "Mechanical";

        if(ui->checkBox_electronical->isChecked())
        {
            if(type.length())
                type += " & ";

            type += "Electronical";
        }

        if(ui->checkBox_smaravel->isChecked())
        {
            if(type.length())
                type += " & ";

            type += "Transistor";
        }

        if(ui->checkBox_was_built_yes->isChecked())
            temp.setWasBuilt(true);

        else if (ui->checkBox_was_built_no->isChecked())
            temp.setWasBuilt(false);

        temp.setName(ui->input_name->text().toStdString());
        temp.setType(type);
        temp.setYearBuilt(ui->input_year_built->text().toStdString());

       if(editMode)
       {
           editComputer.setName(temp.getName());
           editComputer.setType(temp.getType());
           editComputer.setYearBuilt(temp.getYearBuilt());
           editComputer.setWasBuilt(temp.getWasBuilt());
           serviceLayer.editComputer(editComputer);
       }
       else
           serviceLayer.addComputer(temp);

       this->close();
    }
}

void addcomputerwindow::clearAddComputerError()
{//Clear all error labels
    ui->label_error_name->clear();
    ui->label_error_type->clear();
    ui->label_error_year_built->clear();
    ui->label_error_was_built->clear();
}

bool addcomputerwindow::computerInputIsValid()
{
    clearAddComputerError();

    bool isValid = true;

    if(ui->input_name->text().isEmpty())
    {//if no input
        ui->label_error_name->setText("<span style='color: red'>Name cannot be empty</span>");
        isValid = false;
    }

    if(!(ui->checkBox_mechanical->isChecked()) && !(ui->checkBox_electronical->isChecked()) && !(ui->checkBox_smaravel->isChecked()))
    {//if no type is checked
        ui->label_error_type->setText("<span style='color: red'>Input not given</span>");
        isValid = false;
    }

    if(ui->input_year_built->text().isEmpty())
    {//if no input
        ui->label_error_year_built->setText("<span style='color: red'>Input not given</span>");
        isValid = false;
    }

    else if(!isAllDigits(ui->input_year_built->text().toStdString()))
    {//If invalid characters are in input
        ui->label_error_year_built->setText("<span style='color: red'>Invalid characters</span>");
        isValid = false;
    }

    if(!ui->checkBox_was_built_no->isChecked() && !ui->checkBox_was_built_yes->isChecked())
    {//if either checkbox for wasBuilt was not checked
        ui->label_error_was_built->setText("<spawn style='color: red'>Input not given</span>");
        isValid = false;
    }

    return isValid;
}

void addcomputerwindow::on_pushButton_cancel_clicked()
{
    this->close();
}

void addcomputerwindow::on_checkBox_was_built_yes_clicked()
{
    if(ui->checkBox_was_built_no->isChecked())
        ui->checkBox_was_built_no->setChecked(false);
}

void addcomputerwindow::on_checkBox_was_built_no_clicked()
{
    if(ui->checkBox_was_built_yes->isChecked())
        ui->checkBox_was_built_yes->setChecked(false);
}
