#include "addscientistwindow.h"
#include "ui_addscientistwindow.h"

addScientistWindow::addScientistWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addScientistWindow)
{
    ui->setupUi(this);
    editMode = false;
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);
    ui->checkBoxAliveYes->setChecked(true);
    on_checkBoxAliveYes_clicked();
}

addScientistWindow::~addScientistWindow()
{
    delete ui;
}

void addScientistWindow::setEditMode(Scientist scientist)
{
    editMode = true;
    editScientist = scientist;

    ui->input_name->setText(scientist.getName().c_str());
    ui->input_birth_year->setText(scientist.getDateOfBirth().c_str());

    if(editScientist.getGender() == "Male")
        ui->checkBoxMale->setChecked(true);
    else
        ui->checkBoxFemale->setChecked(true);

    if(scientist.getDateOfDeath() == "Alive")
        ui->checkBoxAliveYes->setChecked(true);
    else
    {
        ui->checkBoxAliveNo->setChecked(true);
        ui->checkBoxAliveYes->setChecked(false);
        ui->input_death_year->setHidden(false);
        ui->label_death_year->setHidden(false);
        ui->input_death_year->setText(editScientist.getDateOfDeath().c_str());
    }
}

void addScientistWindow::on_pushButton_ok_clicked()
{
    if(ScientistInputIsValid())
    {
        Scientist temp;
        std::string gender;

        if(ui->checkBoxMale->isChecked())
            gender = "Male";
        else
            gender = "Female";

        if(ui->checkBoxAliveYes->isChecked())
            temp.setDateOfDeath("Alive");
        else
            temp.setDateOfDeath(ui->input_death_year->text().toStdString());

        temp.setName(ui->input_name->text().toStdString());
        temp.setGender(gender);
        temp.setDateOfBirth(ui->input_birth_year->text().toStdString());

       if(editMode)
       {
           editScientist.setName(temp.getName());
           editScientist.setGender(temp.getGender());
           editScientist.setDateOfBirth(temp.getDateOfBirth());
           editScientist.setDateOfDeath(temp.getDateOfDeath());
           serviceLayer.editScientist(editScientist);
       }
       else
           serviceLayer.addScientist(temp);

       this->close();
    }
}

void addScientistWindow::clearAddScientistError()
{
    ui->label_error_name->clear();
    ui->label_error_gender->clear();
    ui->label_error_birth_year->clear();
    ui->label_error_death_year->clear();
}

bool addScientistWindow::ScientistInputIsValid()
{
    clearAddScientistError();
    
    bool isValid = true;

    if(ui->input_name->text().isEmpty())
    {
        ui->label_error_name->setText("<span style='color: red'>Name cannot be empty</span>");
        isValid = false;
    }

    if(!(ui->checkBoxMale->isChecked()) && !(ui->checkBoxFemale->isChecked()))
    {
        ui->label_error_gender->setText("<span style='color: red'>Gender cannot be unchecked</span>");
        isValid = false;
    }

    if(ui->input_birth_year->text().isEmpty())
    {
        ui->label_error_birth_year->setText("<span style='color: red'>Please enter the year of birth</span>");
        isValid = false;
    }

    else if(!isAllDigits(ui->input_birth_year->text().toStdString()))
    {
        ui->label_error_birth_year->setText("<span style='color: red'>Years are represented in numbers</span>");
        isValid = false;
    }

    if(ui->checkBoxAliveNo->isChecked())
    {
        if(ui->input_death_year->text().isEmpty())
        {
            ui->label_error_death_year->setText("<span style='color: red'>Please enter the year of death</span>");
            isValid = false;
        }

        else if(!isAllDigits(ui->input_death_year->text().toStdString()))
        {
            ui->label_error_death_year->setText("<span style='color: red'>Years are represented in numbers</span>");
            isValid = false;
        }

        else if((ui->input_death_year->text().toInt()) < (ui->input_birth_year->text().toInt()))
        {
            ui->label_error_death_year->setText("<span style='color: red'>A person cannot die before she is born</span>");
            isValid = false;
        }

    }
    
    return isValid;
}

void addScientistWindow::on_pushButton_cancel_clicked()
{
    this->close();
}

void addScientistWindow::on_checkBoxMale_clicked()
{
    if(ui->checkBoxFemale->isChecked())
        ui->checkBoxFemale->setChecked(false);
}

void addScientistWindow::on_checkBoxFemale_clicked()
{
    if(ui->checkBoxMale->isChecked())
        ui->checkBoxMale->setChecked(false);
}

void addScientistWindow::on_checkBoxAliveYes_clicked()
{
    if(ui->checkBoxAliveNo->isChecked())
        ui->checkBoxAliveNo->setChecked(false);

    if(!ui->checkBoxAliveYes->isChecked())
        ui->checkBoxAliveYes->setChecked(true);

    ui->input_death_year->setHidden(true);
    ui->label_death_year->setHidden(true);
}

void addScientistWindow::on_checkBoxAliveNo_clicked()
{
    if(ui->checkBoxAliveYes->isChecked())
        ui->checkBoxAliveYes->setChecked(false);

    if(!ui->checkBoxAliveNo->isChecked())
        ui->checkBoxAliveNo->setChecked(true);

    ui->input_death_year->setEnabled(true);
    ui->input_death_year->setHidden(false);
    ui->label_death_year->setHidden(false);
}
